import Vue from 'vue'
import './plugins/axios'
import App from './App.vue'
import router from './router'
import store from './store'
// import $ from "jquery";
import './registerServiceWorker'
import './plugins/element.js'
import '@/assets/css/public.css' // 公共css
import '@/assets/icon/iconfont.css' // 公共图标字体
Vue.config.productionTip = false

new Vue({
  router,
  store,
  render: h => h(App),
  data() {
    return {
      DOMAIN: 'https://junction.dev.havensphere.com/api/junctioncore/v1',
      BASEDATA: {
        hotel_id: '086test1',
        restaurant_id: 'r003',
        device_id: 'stephen_dev_pc_v1',
        device_type: 'tv'
      }
    }
  },
  methods: {
    imgFormatting(imgUrl, callback) {
      callback = callback || function() {}
      this.axios
        .get(imgUrl, {
          responseType: 'arraybuffer'
        })
        .then(res => {
          callback(
            'data:image/png;base64,' +
              btoa(
                new Uint8Array(res.data).reduce(
                  (data, byte) => data + String.fromCharCode(byte),
                  ''
                )
              )
          )
        })

        .catch(err => {
          console.error(err)
        })
    }
  }
}).$mount('#app')
